"use strict";
/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 03-classes.ts
* Executação node 03-classes.js
* Documentação: https://www.typescriptlang.org/docs/handbook/classes.html
*/
var Stark = /** @class */ (function () {
    function Stark() {
        this.name = "Brandon";
        this.saying = "Winterfell!";
    }
    Stark.prototype.hello = function (person) {
        console.log("Hello, " + person);
    };
    Stark.castle = "Winterfell";
    return Stark;
}());
var ned = new Stark();
ned.saying = "Winter is coming";
// console.log(Stark.castle)
ned.hello("Robert");
