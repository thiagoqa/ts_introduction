/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 02-interfaces.ts
* Executação node 02-interfaces.js
* Documentação: https://www.typescriptlang.org/docs/handbook/interfaces.html
*/

// ==> Interfaces
// ? antes da variavel torna como opcional o parametro
interface stark {
    name: string;
    age?: number;
}

function printName(stark : stark ) {
    console.log(stark.name);
}

printName({name:"Eddard"});
printName({name:"Joe"});
