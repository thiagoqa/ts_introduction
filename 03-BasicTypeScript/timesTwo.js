"use strict";
var Utility;
(function (Utility) {
    var userful = /** @class */ (function () {
        function userful() {
        }
        userful.prototype.timesTwo = function (n) {
            return n * 2;
        };
        return userful;
    }());
    Utility.userful = userful;
})(Utility || (Utility = {}));
