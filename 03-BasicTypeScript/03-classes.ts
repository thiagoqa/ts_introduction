/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 03-classes.ts
* Executação node 03-classes.js
* Documentação: https://www.typescriptlang.org/docs/handbook/classes.html
*/

class Stark {
    name: string = "Brandon";
    static castle: string = "Winterfell"
    saying: string;

    constructor(){
        this.saying = "Winterfell!";
    }

    hello(person:string){
        console.log("Hello, " + person)
    }
}

var ned = new Stark();
ned.saying = "Winter is coming";
// console.log(Stark.castle)
ned.hello("Robert");
