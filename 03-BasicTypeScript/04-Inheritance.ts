/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 04-Inheritance.ts
* Executação node 04-Inheritance.js
* Documentação: https://www.typescriptlang.org/docs/handbook/classes.html#inheritance
*/

class Person {
    name: string;
    constructor(name:string) {
        this.name = name;
    }
    dance(){
        console.log(this.name + " is dancing");
    }
}

var bran = new Person("Bran");
bran.dance();

class AwesomePerson extends Person {
    dance(){
        console.log("SO AWESOME!");
        super.dance();
    }
}

var robb = new AwesomePerson("Robb");
robb.dance();
