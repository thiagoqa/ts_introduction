var Utility;
(function (Utility) {
    var userful = /** @class */ (function () {
        function userful() {
        }
        userful.prototype.timesTwo = function (n) {
            return n * 2;
        };
        return userful;
    }());
    Utility.userful = userful;
})(Utility || (Utility = {}));
/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 05-Modules.ts
* Executação node 05-Modules.js
* Documentação: https://www.typescriptlang.org/docs/handbook/modules.html
*/
/// <reference path="timesTwo.ts"
var use = new Utility.userful();
console.log(use.timesTwo(9));

