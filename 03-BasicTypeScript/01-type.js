"use strict";
/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 01-type.ts
* Executação node 01-type.js
* Documentação https://www.typescriptlang.org/docs/handbook/basic-types.html
*/
// ==> Type system
// Example: var n : string = 1;
// Error Type '1' is not assignable to type 'string'.
var n = 1;
n = 'Thiago';
console.log(n);
// ==> Basic types
var isWinter = false;
// Error Type '1' is not assignable to type 'string'.
// isWinter = 123; 
var count = 5;
// declare const name: never;
// var name : string = "Bran";
// user
var names = ["Jon", "Rickon", "Arya"];
// or
var Starks;
(function (Starks) {
    Starks[Starks["Jon"] = 0] = "Jon";
    Starks[Starks["Bran"] = 1] = "Bran";
    Starks[Starks["Eddard"] = 2] = "Eddard";
    Starks[Starks["Catlyn"] = 3] = "Catlyn";
})(Starks || (Starks = {}));
;
var cat = Starks.Catlyn;
function getName() {
    console.log("Winter is coming!");
}
getName();
