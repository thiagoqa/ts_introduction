/*
* Transpilação do arquivo automaticamente usando visual studio code: CTRL + SHIFT + B -> tsc: build/watch
* Manualmente tsc + 01-type.ts
* Executação node 01-type.js
* Documentação https://www.typescriptlang.org/docs/handbook/basic-types.html
*/

// ==> Type system

// Example: var n : string = 1;
// Error Type '1' is not assignable to type 'string'.
var n : any = 1;
n = 'Thiago';
console.log(n)

// ==> Basic types
var isWinter : boolean = false;
// Error Type '1' is not assignable to type 'string'.
// isWinter = 123; 

var count : number = 5;
// declare const name: never;
// var name : string = "Bran";
// user
var names : string[] = ["Jon", "Rickon", "Arya"];
// or
enum Starks {Jon, Bran, Eddard,Catlyn};

var cat : Starks = Starks.Catlyn;

function getName(): void{
    console.log("Winter is coming!");
}
getName()
