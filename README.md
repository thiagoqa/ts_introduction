# Introduction to type script

Code developed during the course [Introduction to TypeScript](https://www.udemy.com/course-dashboard-redirect/?course_id=468138)

## Requirements

* **[Node.js](https://nodejs.org/en/)**
* **[TypeScript](https://www.typescriptlang.org/download)**
* **[Visual Studio Code](https://code.visualstudio.com/)**

## Execution

* node + name_file.js
